﻿Player:You turn around in bed as you hear footsteps coming up.
If(Slime_Route)
	#SlimeH1b
Else 
	Player:After a few seconds, the slime sisters and {Villager} barge inside.
	Player:"Would you be so kind to wait while I get ready?"
	Player:"Prove your good manners, it's not normal to barge in on people sleeping."
	BigSlime:"Can I see your alarm-cock?" She tries to pull up the blanket as her sister stops her.
	SmallSlime:"Stop immediately, what are you thinking?"
	BigSlime:"Ehehehe~" She just giggles.
	Villager:"Why would you even ask?"
	BigSlime:"Want me to join in with you?"
	Player:The others just staring at her she bows over your bed.
	Villager: She turns around facing the door. "We'll wait for you to get ready." She leaves the room.
	SmallSlime:"And while he's undressing, you'll come with us!" She takes her sisters arm and drags her outside.
	BigSlime:"Awwwwww!"
	Player:They step outside and close the door.
	Player:You hurry towards a chair, picking up your clothes and quickly dressing up.
	Player:As you're finished you take a look at the clock, it seems around 5 o'clock.
	Player:<<Wonder what they are up to this early.>>
	Player:You pack your usual equipment and walk down the stairs.
	Player:The three girls already sitting on the couch, you join them. "Morning."
	BigSlime:"Are you sure we aren't troubling you?"
	Player:"Sure you are." You let out a small sigh. "But since the horse has already bolted, why not just tell me what brought you here?"
End
Villager:"I've a small favor to ask you." She lowers her head.
