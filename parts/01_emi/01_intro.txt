narrator:You wake up to the sun shining in your eyes.
narrator:As you open your eyes, you suddenly remember that you intended you meet {creeper} today.
narrator:You get up, pull over your clothes, and hurry outside.
narrator:As you reach the lake you planned to meet her, the area was already crowded.
narrator:Crowded with trees. Nobody else was there.
narrator:You look up into the bright clear sky and try to find out which time it is.
narrator:Judging from the sun, it's around mid-noon.
narrator:You walk around the small lake and look to the woods.
narrator:After all, you knew that your friends usually shun direct sunlight.
narrator:But there was nobody here.
narrator:Feeling a bit disappointed, you step between the dense trees.
narrator:Without anything else left to do, you try visiting your previous home.
narrator:After all, it was just a few minutes away, nothing compared to the distance you already have covered.
narrator:Walking through the woods, you suddenly notice that it's quieter than usual.
narrator:Normally, you could at least hear the wind rustling through the leaves, but today they remained calm.
narrator:You look down onto the ground and miss your shadow.
narrator:Since the tree canopy is too dense, you don't give it a second thought.
narrator:After all, it was you who designed the whole area.
narrator:You are the one who shapes this world.
narrator:After a few minutes dwelling on your thoughts you finally reach a small hut, hidden away between the oldest of the trees.
narrator:Again you feel like you should take them down, but somehow you don't have the heart to do it.
narrator:You decide to look around for stuff you might have left behind the time you had to leave the house rather rashly.
narrator:You just didn't feel safe around this place anymore.
narrator:You slowly open the door and step inside.
narrator:In the instant you step onto the creaking wooden floor, you feel at home.
narrator:It's been a while, but it was the first place you ever settled down.
narrator:You turn on the lights, and to your relief the room becomes alit with a bright yellow.
narrator:You walk upstairs and rummage around in your chests.
narrator:It's mostly garbage, but you pick up a few notes you made when you first awoke in this world.
narrator:You simply don't feel like leaving it all behind.
narrator:At least, not yet.
narrator:It's your home, but still you'd like to know more about the world and your relation to it.
narrator:It's like {villager} surely would remind you:
## request sprite
#show "villager", "normal/fuzzy/oc"
narrator:"Knowledge is the ultimate weapon."
#hide "villager"
narrator:You shake away the thoughts and decide to pursue them later.
narrator:You close the chests again and walk back down the stairs, facing a small hole in the ground.
narrator:You know that it contains a ladder, leading deep into the underground.
narrator:You turn backwards and slowly put your feet on the top rung.
narrator:Slowly taking one by another you climb down.
narrator:As you jump down the last 5 steps, you hold your breath for a few seconds.
narrator:But you can't hear a thing, save for lava bubbling in the distance.
narrator:You decide to go on and turn to your right.
narrator:You inspect the maps you hung at the wall, and decide that only one area was worth visiting.
narrator:Looking at the map you once again note that the edges are charred.
narrator:You decide to never again let a blaze 'touch' your maps.
narrator:You shrug your shoulders and continue along.
narrator:After a while, you reach a small set of cobblestone stairs, leading you further down into the entrails of this world.
narrator:You shudder at the thought, and somehow wish {creeper} was here to tag along.
narrator:Feeling a bit uneasy, you step ahead.
narrator:As you continue downwards, you notice the air surrounding you was getting thicker.
narrator:It's been a while since you've been so deep, and you decide to hurry up in case you should meet anything.
narrator:At a small branch you turn left.
narrator:And stand in the forest again.
narrator:It takes you a few minutes to realize this was your first attempt to farm wood.
narrator:And you still know how many skeletons had to lose their life to make plants grow down here.
narrator:It wouldn't matter to you, if it wasn't for {skeleton} who you met a few weeks later.
narrator:You still remember the first time you explained her how you were able to grow trees underground.
## request sprite
#show "skeleton" "surprised/fuzzy/oo"
narrator:Also, you'll probably never forget her face when she realized.
## request sprite
#show "skeleton" "happy/fuzzy/co"
narrator:And the moment she started laughing like maniac and thanking you for doing her a favor.
#hide "skeleton"
narrator:Anyways, it still feels to you like walking through a graveyard.
narrator:As you step through the dense underground forest, you notice the fog becoming denser.
narrator:Maybe it's just your imagination.
narrator:After all, you abandoned this place a long time ago and never returned since.
narrator:Still, you feel like there is something off, like someone is watching you.
narrator:You turn around and only hear the sound of something disappearing.
narrator:Why do you relate that sound with something disappearing again?
narrator:You turn around again and focus on what you came for.
narrator:Not remembering where exactly you placed them, you look around for your chests.
narrator:Suddenly, you see a familiar face in the dark. It almost looks like yours.
player:«A mirror?»
player:«Nah, that can't be, I didn't invent those yet.»
player:«And if the current laws of physics still apply, it can't be a vertical lake either.»
player:You shake your head in confusion, but as you open your eyes the figure is already gone.
narrator:You look around the place you saw it.
narrator:A small crumpled note catches your attention.
narrator:"{player}, don't you dare forget your quest. Go home and prepare yourself, for unknown events are to come."
narrator:You look at the paper for a few seconds, and once again get the feeling of being stalked.
narrator:You look over your shoulder, but there's nobody there.
narrator:Fed up with the silence and the fog, you decide to return to the surface.
narrator:You climb back up, and leave your old house a few minutes later.
narrator:You decide it's for the best if you went home; maybe the note actually had meaning.
narrator:But mainly because you hope {creeper} was waiting for you there.
narrator:After all, she told you to meet her at the lake, but maybe something happened.
narrator:As you step out into the muggy summer air, you notice the grass here being covered in a soft layer of fog, too.
narrator:You close the door and walk back towards the lake.
narrator:To your surprise, a familiar figure waits for you.
silverfish:"Hey, brother, what are you doing here?"
player:"I was revisiting my old home after {creeper} didn't show up to our date."
player:"I should be asking you the same, what are you doing here?"
silverfish:"Uh, not stalking you or anything. Just in case you wondered~" She giggles.
silverfish:"But since someone broke the spawner we have no way to return to our stronghold for now."
silverfish:Her face turns into a devious smile for a second.
silverfish:"Anyway, I'm free for today and just want to roam the earth!"
silverfish:"It's nice to leave the caves once in a while, riight?"
silverfish:"But you have yet to tell me what brings *you* here."
player:"Didn't I already tell you in the beginning?"
silverfish:She tilts her head, contemplating. "Oh yeah, totally forgot about that."
player:You sigh. "Can it be true that your memory is similar to that of a goldfish?"
silverfish:"A what?" She raises a brow.
player:"A... Ah, forget it."
narrator:In your mind you make a note to better think about what you say in the future.
narrator:Sometimes words enter your mind, words that you thought you'd long since forgotten.
narrator:But she can't know.
silverfish:"Okeee~!" Her lighthearted response interrupts your train of thought.
narrator:Sometimes you can't help but see her as nothing more but a little girl.
narrator:Even if she probably isn't.
narrator:Still, it feels like she's your little sister.
player:"Anyway, I was on my way home. Want to tag along?"
silverfish:"Mhm." She happily nods and takes your hand.
player:"Say, how come you are so happy today?"
silverfish:"Ah nothing, nothing. You know how I am."
narrator:You remember that she always seemed a bit energetic, but judging by her appearance, you don't find it surprising she acts like a child.
silverfish:"Soo, are we going or what?"
narrator:As you take her hand, her fingers wrap closely around yours.
narrator:You start walking towards the place you call your home together.
narrator:As you reach your house, you find that the door is ajar.
narrator:You are certain that you closed it, since you don't want mobs to infiltrate this house, too.
narrator:Standing in front the entrance, {silverfish} suddenly pushes you.
silverfish:"What'cha waiting for? Go ahead!"
narrator:She continues trying to get you inside.
player:"Do I look like a ghost to you? Also-"
narrator:With a loud crash the door collapses and you both fall into the hallway.
unknown:"Dramatic performance, I'll give you that."
unknown:"Anyways..."
creeper:"Happy spawnday!"
player:"Uh, what's going on here? Spawnday?"
creeper:"I'd call it a birthday, but since I have no clue what 'birth', I can't do that right?" She giggles.
smallslime:"Anyways, we decided to have a small party to celebrate your appearance in this world."
skeleton:"It's always nice to have a friend that likes to murder people, I-"
narrator:{creeper} slaps her.
creeper:"Shut up, you creep!"
skeleton:"You're one to talk! Isn't blowing others into pieces your only reason of living?"
villager:"Please girls, calm down for a bit."
villager:"Please refrain from such discussion in the presence of our guest." She gives a bow into your direction.
narrator:Both of them turn silent and face you again.
creeper:She lowers her head a bit. "I'm sorry."
creeper:"Anyways, let's have a party!" She throws her arms into the air.
player:"Well, the surprise come off perfect, I'll give you that."
player:"But how did you get that Herobrine guy?"
creeper:"What do you mean?" She and the others look at you in confusion.
creeper:"I never heard this name before."
narrator:You explain what happened.
creeper:"We just distracted you by sending you to a place to meet us..."
creeper:"While we would prepare a small surprise for you." She giggles.
villager:"I asked {silverfish} to stalk you."
villager:"You know, as silverfish she can easily avoid your detection."
player:"That would at least explain my paranoia huh?"
bigslime:"Normally you can feel when you are being watched!"
smallslime:She turns around and watches {silverfish}. "Though I'd still like to know how exactly you did that."
smallslime:"I mean, aren't you a bit too, uh, big to fit in a single block?"
silverfish:"Nah, you see how small and delicate I am!"
bigslime:"Not to mention flat."
bigslime:"You sure you are a girl?"
bigslime:She smirks while displaying her chest in a provocative manner.
silverfish:"You say that like it's fun to carry those udders!"
smallslime:She giggles while her sister puffs out her cheek.
narrator:{creeper} turns around and faces {silverfish}.
creeper:"Also, what THE HELL were you doing?"
creeper:"Didn't we have a plan or something!?"
silverfish:"Eeeek!" {creeper}'s shout made her cringe.
silverfish:"Just following people around is boring, ok?" She sulks and lowers her head to the floor.
player:"It's fine." You can't help yourself but to come to her defense.
player:"I was glad to have someone to walk home with."
narrator:{creeper} pouts while {silverfish} sticks out her tongue in a childish manner.
narrator:{creeper} moves a bit closer and lowers her voice.
creeper:"Also, I'd like to tell you that this wasn't my idea at all."
creeper:"Sorry, I didn't intend on letting you wait."
creeper:She pouts and turns her back on you. "I just wanted to have a private date and give you a gift, nothing more."
creeper:"Buuut..." She turns around again and regains her composure. "A birthday party is a nice surprise too, isn't it?"
player:"So, what do I get for my birthday?"
bigslime:"Me obviously." She looks at you seductively.
smallslime:"Around 100kg of slime as gift?"
smallslime:"Sounds like an idea only you could dream up."
bigslime:"You think he likes a board-like slime better!?"
narrator:They both look sharply at each other.
player:You let out a sigh, "Calm down girls, I don't want either of you."
smallslime:"That doesn't make it any better!"
player:"Well, at least this time you have the same point of view."
silverfish:"Also, also, we baked you a cake!"
silverfish:"A cake-y cake!" Giving you a huge grin she points at the desk behind {bigslime}.
smallslime:"Out of the way, fatty!"
narrator:{bigslime} seems to prepare a counter for a second, then moves out of your sight.
narrator:In front of you is standing a huge cake.
player:"Wow, how did you make that?"
villager:"Hehehe, did you already forgot that my sisters and I are literally masters in growing crops?"
creeper:"That's only because you literally can't do anything else, duh."
villager:"Well, we surely can't do everything."
villager:"For example, giving your stupid creeper a bit of respect!" {creeper} pouts.
villager:"Uh-hum," She clears her throat, "What I wanted to say is, we can grow crops and bake cake."
silverfish:"Also, also," She begins to tug at your shirt, "How old are you now?"
player:You try to remember your past, but it's all in vain. "As far as I can remember, like 22."
silverfish:"Wow, that's like 3 years older than me!" She watches you in awe, then her face turns into a pout.
silverfish:"After all, I'm the youngest around..."
player:"Don't worry, you're perfect like you are." You pet her head gently and her mood brightens in an instant.
villager:"Not remembering the past sounds like something you should work on."
villager:"I, for my part, have dedicated my life to studying everything there is."
villager:"There is no better way to deal with this world but to accumulate knowledge."
villager:"It's our ultimate weapon."
villager:"Also, regarding your story: Did you know that fog is the same material as the clouds?"
villager:"So, you basically were like walking through clouds."
bigslime:"Ahaha clouds from under the earth?"
bigslime:"Must be on their way to hell or something."
player:"Wait a second. How do you know it was under earth?"
bigslime:"That's obvious, where else would you find clouds?"
smallslime:"That's like, the worst argument I've ever heard..."
narrator:With the girls continuing their argument, you decide to leave the house in search for {creeper}.
player:After a brief search, you find her cowering behind a tree. "What are you doing here?"
creeper:"Uaah?!" She jerks at your words and swiftly turns around. "W-What are you scaring me for, idiot?!"
player:"Just for fun." You put on a mean grin as she slowly pulls herself up. "Just so you know how that feels like~"
creeper:"Mhpf~" Her face turns into a pout as she pulls her hoodie down her face. "Whatever."
player:"Oh, oh, no need to blush!" You give her a smile as you grab her hand. "Why aren't you celebrating with the others?"
creeper:"Already told you I'd rather not join a bunch of carefree girls on a party." Her expression darkens.
player:"You don't have to." You give her a shrug as you turn around.
player:"They won't stay too long though. If you want, why not come back later?"
creeper:"Sure why not. Maybe I'll watch you while you fall asleep again~" A shiver runs down your spine as she lets out a girlish giggle. "See yaaaa~"
narrator:A bit unnerved, you walk back to your house, and as you turn around, like usual, she's gone.
player:As soon as you enter the house, {silverfish} runs up to you. "Waaaaaah!"
silverfish:"Where you've been stupid!?" With a scolding look, she grabs your hand. "It's dark outside!"
silverfish:"If you want to go outside at an hour like this, you need a light!"
silverfish:"Here you can have one from me!" She walks towards the wall next to you and tries to grasp a torch, but it's hanging too high.
silverfish:"Eeeeh!" With a disappointed look, she lets herself fall on her knees. "If it wasn't sticking to the sky, that is..."
player:"What are you doing, trying to disassemble my house?" You give her a soft slap on her head. "That's not 'your' torch dummy."
silverfish:"But I-" You interrupt her with a wave from your hand.
player:"I'd rather have you go for the cake, no need to hold back." You reach her a hand and pull her up.
player:"As you might have noticed, I'm already back again, unscathed too."
silverfish:"Oh... right." Watching you with a dumbfounded expression, she lays a finger on her lips and starts to giggle.
silverfish:"Right, the cake it is~" She turns around, pulling you towards the kitchen. "Needa hurry before the fatty gets it!"
silverfish:Just as you hurried past {bigslime}, she stops and turns her head. "Hi fatty!"
bigslime:She grimaces as she puts a hand on her forehead. "How many more times are you gonna greet me like this?!"
silverfish:"Dunno, just like the term 'fatty', it suits you~" She points our her tongue as {bigslime} starts to blush.
silverfish:"Also-" She hesitates for a moment. "- my cake!" Turning around she rushes towards the table and pounces at the remaining half of your birthday cake.
player:"Don't you wanna learn how to bake?" You give {bigslime} a grin. "I bet you'd surely win *her* heart, at least."
bigslime:"Me, in the kitchen!?" In outrage she puts her arms akimbo. "Do I look like I'd make a good housewife!?"
bigslime:"Wait..." She lowers her arms as she stares at the floor. "That came out wrong. I'd make a *perfect* housewife."
player:"I, uh, think?" As {bigslime} raises her head and looks into your eyes again, you notice her head turning deep red.
player:"B-By the way, where are the others?" You look around yourself, but most of the girls are gone.
silverfish:"Uhm-mh~" She turns around with bloated cheeks. "Fe fime fifters-"
player:"How about you finish your cake first?" You give her a scolding look as she swallows with a grin on her face.
player:"Or rather, my cake."
silverfish:"Cake? What cake?" She gives you an innocent expression, and as you look past her, you only see an empty plate.
silverfish:"Hey, if you got cake, then I too want a piece!" Beginning to pout she puts her arms akimbo.
villager:"There is no cake." {villager} steps up to you and gives {silverfish} a slap.
villager:"You know, some of us didn't have cake yet."
silverfish:"But" She puts on a cheeky smile. "To make cake you only need eggs and milk!"
silverfish:"Since you got spawn-eggs, and are friends with a cow..." She quickly glances at {bigslime}. "I figured that would be no problem."
villager:"Mostly." She gives her a glare. "But you also need wheat, and *somehow* our wheat vanished overnight!"
villager:"Which reminds me, I still didn't punish you for that." She raises a brow, while remaining unexpectedly calm. "And I know a *lot* of punishments."
silverfish:"That was autumn's fault!" She begins to sulk as she sends you a begging look.
silverfish:"Geez, I don't even know how to harvest!" With a defiant expression she folds her hands in front of her chest.
bigslime:"My, my." Stepping in between the fighting girls, she shakes her head.
bigslime:"You know, {silverfish}, the fields are not a playing ground for little girls."
villager:"Especially not if they carry buckets, got that?"
silverfish:"Geez, how would I know the waters would be so many!" She turns around in a huff.
player:"Uh, would you mind telling me what happened?" Trying to defuse the situation you pull out {silverfish}.
silverfish:"Uuuuh..." She averts her looks as you grab her hand.
silverfish:Turning around, she gives you a grin. "I just played with one of your buckets on the fields in the village."
silverfish:Her face turns into an expression of sorrow. "But somehow I tripped, and I spawned a flood out of nowhere!"
silverfish:"You know, when you never did it before, it's not easy to clean up a bunch of lava- uuuh, I mean water~ Eheheee~" With a bad pokerface she lets out an embarrassed giggle.
silverfish:Giving you a perky wink, she turns around and focuses {villager}. "You know {villager}, I was just trying to get knowledge."
silverfish:"What you call merely fooling around, was a thoughtfully schemed operation regarding the reality of crops, Mhmhm~"
silverfish:Giving her a confident nod, {villager} raises a brow. "So you literally can't get mad at me, got that?"
villager:"Well, seen from that angle..." Visibly unsettled she lays a finger on her mouth. "Though I wouldn't assume you'd have the required mental capabilities for such testing."
villager:"Well, if that's the case," She gives {silverfish} a wink as her expression turns into a triumphant smile. "I think I can let you off the hook."
villager:"You can prove it sometime during the next few days by coming to our village and helping me make a few potions"
silverfish:"Sure." She shrugs. "Can't be *that* hard."
villager:"Well, do you remember that crater a kilometer behind your fortress?"
silverfish:She tilts her head in confusion. "Yeah, why?"
villager:Not responding, she begins to grin as she walks up to you.
villager:"Don't worry." She stops next to you and whispers into your ear. "Just joking there, should teach her a lesson."
silverfish:With a puzzled expression her looks alternate between the leaving {villager} and you. "What did that nerd mean?"
player:Not surprised that {silverfish} didn't get her joke, you give her a shrug, "Don't ask me."
silverfish:"Well then." Returning to her usual mood, she gives you a cheerful smile. "At least the potions can't explode or something~"
silverfish:"Also, I still need to tell you something, but I got interrupted earlier!"
silverfish:"Wanna hear it, wanna hear it?" With a gleaming smile she folds her hands in front of her chest.
player:"Fire away."
silverfish:"Yay, I'm a messenger!"
silverfish:"Here it comes!" She clears her throat, then proudly builds herself up in front of you.
silverfish:"The slime sisters seem to have headed home already!"
bigslime:"My, my, the first day being a messenger and already going to get fired!"
bigslime:She gives her a nudge to the side. "I'm still here, stupid!"
silverfish:"Oh, yeah, you're right." She eyes in surprise. "I mean, left."
silverfish:"Sorry, kinda mistook you for a cow again."
bigslime:"Thehehe~" Not getting the anticipated response, {silverfish} takes a step backwards as {bigslime} licks her lips.
bigslime:"You better take care that I not *accidentally* mistake you for food, or maybe a doormat?"
silverfish:"And you take care that I don't mistake you for a door."
silverfish:"Uh, brother, what's this strange fat door doing in your living room?" she stares at {bigslime} in surprise.
silverfish:"Wonder where it leads!" She gives {bigslime} a few hard knocks on the head. "Knock, knock!"
smallslime:She suddenly enters the room. "Who's theeeere?" Hardly suppressing a giggle she winks at {silverfish}.
silverfish:"It's-a me, {silverfish}!"
bigslime:She chokes back her anger as the two girls stand there burst into laughter.
bigslime:"A-Anyways..." You notice her voice to quiver, "Mind if I stay overnight?"
bigslime:She tries to put on a smile, but it turns out to be some sort of grimace.
player:"Yeah, it's gotten kinda late again..." Instead of punishing your sister, you decide to just fulfill {bigslime}'s request.
player:"I don't mind if you stay overnight, as long as that doesn't mean I gotta lock my door at."
bigslime:"Well," She proudly puffs out her chest, "If you got *me* as sister instead of *her* you would *never* need to lock that door again~"
player:"Whatever."
player:"Well, I guess I still gotta *guest* outside..."
player:"So, while I'm busy, feel free to make yourself feel at home."
bigslime:"Sure." Giving you a wink, she grabs her sister's hand and pulls her towards the hallway.
bigslime:"We're taking a shower, and you know, there's no door to lock!"
smallslime:"Wait, I-"
bigslime:"Nah, Nah. You. Are. Coming. With. Me!" Dragging her struggling sister behind her, she disappears into the hallway. "See ya~"
silverfish:"I'll be preparing the bed!" Without objection, she hurries upstairs, almost tripping on your carpet.
narrator:As everyone left, {creeper} was still waiting for you.
player:Finding her suddenly in front of the window, you signal her to come to the front door. 
creeper:"That was a nice evening."
creeper:"Although I'd preferred to have you for my own..." She gives you a sulky expression.
player:"If you want to, then I'll dedicate tomorrow just to playing with you?"
creeper:"You'd really do that for me?" Her face brightened in an instant.
player:"Sure. But for now let's go to sleep, it was a long day after all."
narrator:You give her a small hug, then she hurries back into the darkness in which she came from.
narrator:Letting out a sigh, you close the door and walk upstairs.
#show "silverfish", "happy/cc"
narrator:You find {silverfish} on the bed, eating cake.
silverfish:"Whath ith it?" She gives you an innocent look as your expression darkens.
silverfish:She swallows. "Told you I was going to prepare the bed. What's a bed without cake, right?"
player:"A bed without a cake is just that." You give her a glare as she spreads crumbs all across the linen. "A Bed."
silverfish:"Hum." Gulping down the last piece, she lets herself fall on the pillow, her belly visibly stuffed.
silverfish:"Could swear you needed at least 10 cakes to craft one."
player:"Nah, that's the recipe for {silverfish}s."
silverfish:"Hey, I need a hundred million at least!" She giggles as you turn around and begin undressing yourself.
player:Used to changing in front of your sister, you don't even give it a thought. "Probably true."
player:"Maybe I'll make you another tomorrow. If you are nice tonight."
silverfish:"Hehehe, the usual {silverfish}-bed is occupied~" She giggles as she swiftly changes into her pajamas.
silverfish:"So tonight it's brother-{silverfish} time~" She crawls into the bed and closes her eyes. "Good night!"
silverfish:Giving a smile at her unusual attitude, you pull up the blanket and place yourself next to her. "Night."
narrator:Promising cake really seemed to help, you make a mental note to yourself to prepare a few dozen chests filled with that solution.
silverfish:"Can {silverfish} still cuddle?" She watches you with big eyes, and you can't help but smile at her.
player:"Sure, dummy." You give her a pat on her head as she cuddles up to you. "Sleep tight."
